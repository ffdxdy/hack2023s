import './App.scss';

import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Landing, Chat, Profile } from './views';
import { store } from './redux/store';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { actions, selectors } from './redux/actions';
import { useEffect, useState } from 'react';

const AuthCheck = ({children}) => {
  const dispatch = useDispatch();
  
  const isAuthorized = useSelector(selectors.isAuthorized);
  const self = useSelector(selectors.getSelf);
  const users = useSelector(selectors.getUsers);
  const rooms = useSelector(selectors.getRooms);

  useEffect(() => {
    if(!isAuthorized){
      dispatch(actions.login({
        login: 'admin@gmail.com',
        password: 'Aaaaaa1@',
      }));
    }
    else {

      if(self === null){
        dispatch(actions.fetchSelf());
      }
      if(users.length === 0){
        dispatch(actions.fetchUsers());
      }
      if(rooms.length === 0){
        dispatch(actions.fetchRooms());
      }
    }
  }, [isAuthorized, self, users, rooms]);


  return children;
};

export const router = createBrowserRouter([
  {
    path: '/',
    element: <Landing />
  },
  {
    path: 'chat/:chatId',
    element: <Chat />
  },
  {
    path: 'profile/:userId',
    element: <Profile />
  }
]);

const App = () => {
  return (
    <Provider store={store}>
      <AuthCheck><RouterProvider router={router} /></AuthCheck>
    </Provider>
  );
}

export default App;
