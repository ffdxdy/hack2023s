import { AppHeader, AppFooter, UserIcon } from '../../components';
import { selectors } from '../../redux/actions';
import './Profile.scss';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const Profile = () => {
  const self = useSelector(selectors.getSelf);
  const { userId } = useParams();

  const selUserId = userId ?? self.id;

  const user = useSelector(selectors.getUsers)
    .find(u => u.id === selUserId) ?? null;
  
  const roleStrings = {
    '0': 'Администратор (L0)',
    '1': 'Работник (L1)',
    '2': 'Стажер (L2)',
  };
  
  const infoRows = user !== null? [
    {label: 'ФИО', info: `${user.surname} ${user.name} ${user.fathername}`},
    {label: 'Отдел', info: user.department},
    {label: 'Должность', info: user.job},
    {label: 'Уровень доступа', info: roleStrings[user.role]}
  ] : [];

  return (
    <>
      <AppHeader />
      <main>
        {user !== null && (
          <div className='profile--wrapper'>
            <div className='profile--avatar-wrapper'>
              <UserIcon userId={user.id} />
            </div>
            <div className='profile--info-rows'>
              {infoRows.map((row, index) => (
                <div className='row-wrapper' key={index}>
                  <div className='label'>{row.label}</div>
                  <div className='info'>{row.info}</div>
                </div>
              ))}
            </div>
          </div>
        )}
      </main>
      <AppFooter />
    </>
  );
};

export default Profile;