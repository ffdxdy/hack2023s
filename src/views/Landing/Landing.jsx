import { AppHeader, AppFooter, NewsContainer } from '../../components';
import { actions, selectors } from '../../redux/actions';
import './Landing.scss';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { api } from '../../redux/api/apiAdapter';

const Landing = () => {
  return (
    <>
      <AppHeader />
      <main>
        <div className='landing--news-wrapper'>
          <NewsContainer
            collapsedContent={<>
              <h1>Микроновость 3</h1>

              <p>Здесь должен быть какой-то текст, но какой именно - неизвестно.</p>
              <p>Что за текст должен быть здесь? Увидим ли мы его? Столько вопросов и никаких ответов.</p>
              <p>Возможно, наука будущего прольет свет на эту загадку.</p>
            </>}
            collapsedBackground='/content/sd2/bgimg-0.jpg'
          />
          <NewsContainer
            collapsedContent={<>
              <h1>Микроновость 14</h1>

              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
              </p>
              <p>It has survived not only five centuries, but also the leap into electronic typesetting, 
                remaining essentially unchanged.
              </p>
            </>}
            collapsedBackground='/content/sd2/bgimg-1.jpg'
          />
          <NewsContainer
            collapsedContent={<>
              <h1>Микроновость 15</h1>

              <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                and more recently with desktop publishing software like Aldus PageMaker including versions of 
                Lorem Ipsum.
              </p>
            </>}
            collapsedBackground='/content/sd2/bgimg-2.jpg'
          />
        </div>
      </main>
      <AppFooter />
    </>
  );
};

export default Landing;