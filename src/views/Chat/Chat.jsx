import { useState } from 'react';
import { useSelector } from 'react-redux';
import { ChatMessage, AppHeader, AppFooter } from '../../components';
import { selectors } from '../../redux/actions';

import './Chat.scss';

const Chat = () => {
  const self = useSelector(selectors.getSelf);
  const roomMessages = useSelector(selectors.getRoomMessages);

  const handleMessageEnter = (event) => {
    if (event.keyCode === 13) {
      event.preventDefault();

      const messageText = event.target.value;

      /*updateMessages(messages => [
        ...messages,
        {isIncoming:false, text:messageText, timestamp:Date.now()},
      ]);*/
      event.target.value = '';
    }
  };

  return (
    <>
      <AppHeader />
      <main>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          {(self !== null && roomMessages !== null) && roomMessages.map((msg) => (
            <ChatMessage
              text={msg.message}
              author={msg.author}
              isIncoming={msg.author !== self.id}
              timestamp={msg.timestamp}
              attachments={msg.attachments}
            />
          ))}
          <input type='text' onKeyDown={handleMessageEnter} />
        </div>
      </main>
      <AppFooter />
    </>
  );
};

export default Chat;
