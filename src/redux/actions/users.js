import { api } from '../api/apiAdapter';

const initialState = {
  isFetching: false,
  lastUpdated: -1,
  knownUsers: [],
  self: null,
};

const getSelf = (state) => state.users.self;
const getUsers = (state) => state.users.knownUsers;

const USERS_FETCH_REQUEST = 'users/fetch--request';
const USERS_FETCH_SUCCESS = 'users/fetch--success';
const USERS_FETCH_FAILURE = 'users/fetch--failure';

const fetchUsers = () => {
  return dispatch => {
    dispatch(fetchUsersRequest());

    api.fetchUsers().then(data => {
      if(data !== undefined)
        dispatch(fetchUsersSuccess({users: data.user}));
      else
        dispatch(fetchUsersFailure());
    });
  };
};

const fetchUsersRequest = () => ({
  type: USERS_FETCH_REQUEST,
});
const fetchUsersSuccess = ({users}) => ({
  type: USERS_FETCH_SUCCESS,
  users,
});
const fetchUsersFailure = () => ({
  type: USERS_FETCH_FAILURE,
});

const USERS_FETCH_SELF_REQUEST = 'users/fetch-self--request';
const USERS_FETCH_SELF_SUCCESS = 'users/fetch-self--success';
const USERS_FETCH_SELF_FAILURE = 'users/fetch-self--failure';

const fetchSelf = () => {
  return dispatch => {
    dispatch(fetchSelfRequest());

    api.fetchUser().then(data => {
      if(data !== undefined)
        dispatch(fetchSelfSuccess({user: data.user}));
      else
        dispatch(fetchSelfFailure());
    });
  };
};

const fetchSelfRequest = () => ({
  type: USERS_FETCH_SELF_REQUEST,
});
const fetchSelfSuccess = ({user}) => ({
  type: USERS_FETCH_SELF_SUCCESS,
  user,
});
const fetchSelfFailure = () => ({
  type: USERS_FETCH_SELF_FAILURE,
});

export const reducer = (state = initialState, action) => {
  switch(action.type){
    case USERS_FETCH_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case USERS_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        lastUpdated: Date.now(),
        knownUsers: action.users,
      };
    case USERS_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    case USERS_FETCH_SELF_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case USERS_FETCH_SELF_SUCCESS:
      return {
        ...state,
        isFetching: false,
        lastUpdated: Date.now(),
        self: action.user,
      };
    case USERS_FETCH_SELF_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};

export const actions = {
  fetchUsers,
  fetchSelf,
};

export const selectors = {
  getSelf,
  getUsers,
};
