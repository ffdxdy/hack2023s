import { api } from '../api/apiAdapter';

const initialState = {
  isFetching: false,
  token: null,
};

const isAuthorized = (state) => state.auth.token !== null;
const getToken = (state) => state.auth.token;

const AUTH_LOGIN_REQUEST = 'auth/login--request';
const AUTH_LOGIN_SUCCESS = 'auth/login--success';
const AUTH_LOGIN_FAILURE = 'auth/login--failure';

const login = ({login, password}) => {
  return dispatch => {
    dispatch(loginRequest());

    api.login({login, password}).then(data => {
      if(data !== undefined){
        api.setAuthToken(data.access);
        dispatch(loginSuccess({token: data.access}));
      }
      else
        dispatch(loginFailure());
    });
  };
};

const loginRequest = () => ({
  type: AUTH_LOGIN_REQUEST,
});
const loginSuccess = ({token}) => ({
  type: AUTH_LOGIN_SUCCESS,
  token,
});
const loginFailure = () => ({
  type: AUTH_LOGIN_FAILURE,
});

export const reducer = (state = initialState, action) => {
  switch(action.type){
    case AUTH_LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        token: action.token,
      };
    case AUTH_LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};

export const actions = {
  login,
};

export const selectors = {
  isAuthorized,
  getToken,
};
