import { api } from '../api/apiAdapter';

const initialState = {
  isFetching: false,
  rooms: [],
  selectedRoom: '6416a82a2615ee2244a107e0',//-1,
};

const getRooms = (state) => state.chat.rooms;

const getRoomMessages = (state) => state.chat.rooms
  .find(r => r.id === state.chat.selectedRoom)
  ?.messages ?? null;
const getRoomMembers = (state) => state.chat.rooms
  .find(r => r.id === state.chat.selectedRoom)
  ?.members ?? null;

const CHAT_ROOMS_FETCH_REQUEST = 'chat/rooms-fetch--request';
const CHAT_ROOMS_FETCH_SUCCESS = 'chat/rooms-fetch--success';
const CHAT_ROOMS_FETCH_FAILURE = 'chat/rooms-fetch--failure';

const fetchRooms = () => {
  return dispatch => {
    dispatch(fetchRoomsRequest());

    api.fetchRooms().then(data => {
      if(data !== undefined)
        dispatch(fetchRoomsSuccess({rooms: data.rooms}));
      else
        dispatch(fetchRoomsFailure());
    });
  };
};

const fetchRoomsRequest = () => ({
  type: CHAT_ROOMS_FETCH_REQUEST,
});
const fetchRoomsSuccess = ({rooms}) => ({
  type: CHAT_ROOMS_FETCH_SUCCESS,
  rooms,
});
const fetchRoomsFailure = () => ({
  type: CHAT_ROOMS_FETCH_FAILURE,
});

export const reducer = (state = initialState, action) => {
  //console.log('chat reducer!')
  //console.log('action:', action)
  switch(action.type){
    case CHAT_ROOMS_FETCH_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case CHAT_ROOMS_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        rooms: action.rooms,
      };
    case CHAT_ROOMS_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
      };
    default:
      return state;
  }
};

export const actions = {
  fetchRooms,
};

export const selectors = {
  getRooms,
  getRoomMessages,
  getRoomMembers,
};
