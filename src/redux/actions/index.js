import { combineReducers } from "redux";

import * as users from './users';
import * as auth from './auth';
import * as chat from './chat';

export const rootReducer = combineReducers({
  users: users.reducer,
  auth: auth.reducer,
  chat: chat.reducer,
});
export const actions = {
  ...users.actions,
  ...auth.actions,
  ...chat.actions,
};

export const selectors = {
  ...users.selectors,
  ...auth.selectors,
  ...chat.selectors,
};
