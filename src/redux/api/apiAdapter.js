const transient = {
  token: null,
};

const catchWrapper = (promise, respMapper = v=>v.json()) => {
  try {
    return promise.then(resp => {
      switch(resp.status){
        case 401: // Refresh needed
          throw new Error("Status 401");
        case 200:
        default:
          return respMapper(resp);
      }
    });
  }
  catch(error) {
    console.group(`API error | ${new Date().toLocaleTimeString()}`);
    console.error('error:', error);
    console.info('data:', promise);
    console.groupEnd();

    return Promise.resolve(undefined);
  }
};

const get = (method, respMapper) => {
  const options = {
    method: 'GET',
  };

  if(transient.token !== null){
    options.withCredentials = true;
    options.credentials = 'include';
    options.headers = {'Authorization': transient.token};
  }

  return catchWrapper(
    fetch(process.env.REACT_APP_BACKEND+'api/'+method, options),
    respMapper
  );
};

const post = (method, payload, respMapper) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(payload),
  };

  if(transient.token !== null){
    options.withCredentials = true;
    options.credentials = 'include';
    options.headers = {'Authorization': transient.token};
  }

  return catchWrapper(
    fetch(process.env.REACT_APP_BACKEND+'api/'+method, options),
    respMapper
  );
};



export const api = {};

api.setAuthToken = token => transient.token = token;

api.login = ({login, password}) => post('auth/login', {
  login,
  password,
  fingerprint: process.env.REACT_APP_FINGERPRINT
});

api.fetchUsers = () => get('user/all');

api.fetchUser = (id) => get(`user${id? `/${id}`:''}`);

api.getAvatarUrl = (userId) => process.env.REACT_APP_BACKEND+'/api/avatar/'+userId;

api.getFileUrl = (relPath) => process.env.REACT_APP_BACKEND+'/api/file?path='+relPath;

api.fetchRooms = () => get('room');
