import { api } from '../../../redux/api/apiAdapter';
import './UserIcon.scss';

const UserIcon = ({ userId }) => {

  return (
    <div className='shared--user-icon'>
      <img src={api.getAvatarUrl(userId)} alt='User icon' />
    </div>
  );
};

export default UserIcon;
