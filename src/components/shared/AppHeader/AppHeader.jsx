import { Icon, UserIcon } from '../../../components';
import { mdiChatOutline } from '@mdi/js';

import './AppHeader.scss';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectors } from '../../../redux/actions';
import { api } from '../../../redux/api/apiAdapter';

const AppHeader = () => {
  const self = useSelector(selectors.getSelf);

  const getDisplayName = ({name, surname, fathername}) =>
    `${surname} ${name[0]}.${fathername[0]}.`;
  
  return (
    <header className='shared--app-header'>
      {self !== null && (
        <>
          <Link to={`/profile/${self.id}`}>
            <div className='user-header'>
              <UserIcon userId={self.id} />
              <div className='user-name'>{getDisplayName(self)}</div>
            </div>
          </Link>
          <Link to='/chat/-1'>
            <div className='chat-icon'>
              <Icon path={mdiChatOutline} size={40} />
            </div>
          </Link>
        </>
      )}
      
    </header>
  )
};

export default AppHeader;
