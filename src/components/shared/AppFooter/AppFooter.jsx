import { Link } from 'react-router-dom';
import './AppFooter.scss';

const AppFooter = () => {
  return (
    <footer className='shared--app-footer'>
      <div className='row'>
        <div className='col'>
          <Link to='/'><div>Какая-то компаниянейм</div></Link>
          <div>О нас? Ничего не расскажем</div>
        </div>
        <div className='col'>
          <div>А ссылки добавим?</div>
          <div>Добавим</div>
          <div>Работающие?</div>
          <div>Работающие</div>
        </div>
        <div className='col'>
          <div>Это строка текста</div>
          <div>И это тоже</div>
          <div>Что вообще</div>
          <div>Здесь ожидают увидеть?</div>
        </div>
      </div>
      
      
      <div>Администратор сайта не несет ответственности за ваши души</div>
    </footer>
  )
};

export default AppFooter;
