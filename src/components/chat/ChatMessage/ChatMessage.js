import { mdiFileDocument, mdiImage, mdiTable } from '@mdi/js';
import { api } from '../../../redux/api/apiAdapter';
import { UserIcon } from '../../shared';
import { Icon } from '../../ui';
import './ChatMessage.scss';

const ChatMessage = ({ text, isIncoming, timestamp, attachments, author }) => {

  const iconWrapper = (
    <div className='chat--icon-wrapper'>
      <UserIcon userId={author} />
    </div>
  );

  const chatMessage = (
    <div className='chat--chat-message'>
      <div className='text'>{text}</div>
      <div className='attachments'>{attachments.map(att => (
        <div className='attachment'>
          <Icon path={{
            'png': mdiImage,
            'jpg': mdiImage,
            'pdf': mdiFileDocument,
            'docx': mdiFileDocument,
            'xls': mdiTable,
          }[att.split('.').at(-1)] ?? mdiFileDocument} size={32} />
          <a
            href={api.getFileUrl(att)}
            download
          >{att.split('/').at(-1)}</a>
        </div>
      ))}</div>
      <time dateTime={timestamp} className='time'>
        {new Date(timestamp).toLocaleString('ru')}
      </time>
    </div>
  );
  
  return (
    <div className={`chat--message-wrapper ${isIncoming? 'incoming':'outcoming'}`}>
      {isIncoming? [iconWrapper, chatMessage] : [chatMessage, iconWrapper]}
    </div>
    
  );
};

export default ChatMessage;
