import './Icon.scss';

const Icon = ({ path, size, className, viewBox, style, onClick }) => {
  return (
    <svg
      className={`ui-icon ${className? className:""}`}
      width={size}
      height={size}
      viewBox={viewBox ?? "0 0 24 24"}
      style={style}
      onClick={onClick}
    >
      <path d={path} />
    </svg>
  );
};

export default Icon;