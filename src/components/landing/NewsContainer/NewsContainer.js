import './NewsContainer.scss';

const NewsContainer = ({ collapsedContent, collapsedBackground, expandedContent }) => {
  
  return (
    <div className='landing--news-container collapsed'
      style={{backgroundImage: `url(${collapsedBackground})`}}
    >
      <div className='content'>{collapsedContent}</div>
    </div>
  )
};

export default NewsContainer;
